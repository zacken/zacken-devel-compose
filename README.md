<!--
SPDX-FileCopyrightText: 2024 Marten Ringwelski
SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# A Composefile for Zacken Development
Zacken requires a bit of setup to get started with development.
This repository provides a compose-file and configuration to get started right away.

## Usage
We provide a `Justfile` that contains recipes for commands you might want to run.
```sh
$ just --list
Available recipes:
    build               # Build the Zacken containers
    clone               # Clone all required repositories
    default             # Clone, build and watch
    down                # Stop and remove containers and networks
    drop-database       # Drop all database contents the
    populate-database   # Populate the Zacken database with sample content
    pytest *PYTEST_ARGS # Run pytest
    start               # Create and start containers
    stop                # Stop containers
    watch               # Start the containers and watch for source changes
```
