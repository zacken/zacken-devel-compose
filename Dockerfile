# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

FROM alpine:3.20 AS zacken

RUN mkdir -p /var/cache/apk && ln -s /var/cache/apk /etc/apk/cache
RUN --mount=type=cache,target=/var/cache/apk \
apk add \
    py3-pip \
    py3-virtualenv \
    poetry \
    # Analyzer Build Dependencies
    make \
    yara \
    # Python Package Build Dependencies
    build-base \
    cmake \
    gcc \
    python3-dev \
    musl-dev \
    libpq-dev \
    linux-headers

RUN python -m venv /venv
ENV VIRTUAL_ENV=/venv \
    PATH=/venv/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

ADD sources/zacken /src/zacken
ENV PYTHONPATH=/src/zacken/ \
    PATH="/src/zacken/:${PATH}"
RUN mkdir /etc/zacken

RUN --mount=type=cache,target=/root/.cache/pypoetry \
cd /src/zacken && \
poetry install --no-root

