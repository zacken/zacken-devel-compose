# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

# Clone, build and watch
default: clone build watch

# Clone all required repositories
clone:
    [ ! -e sources/zacken ] &&  git clone https://codeberg.org/zacken/zacken.git sources/zacken || true

# Run pytest
pytest *PYTEST_ARGS:
    docker compose run \
        --rm \
        --workdir /src/zacken/ \
        --entrypoint pytest \
        zacken-test \
        --postgres-url \
        postgresql://postgres:postgres@postgres:5432/zacken-test \
        --minio-url \
        http://minio:9000 \
        {{PYTEST_ARGS}}

# Build the Zacken containers
build:
    docker compose build

# Stop and remove containers and networks
down:
    docker compose down

# Stop containers
stop:
    docker compose stop

# Create and start containers
start:
    docker compose create
    docker compose start

# Start the containers and watch for source changes
watch:
    docker compose watch

# Drop all database contents
drop-database: stop
    docker compose rm postgres --force
    docker volume rm $(docker compose config --format json | jq --raw-output '.volumes["postgres-data"].name')

# Populate the Zacken database with sample content
populate-database:
    #!/bin/sh
    zacken_db_dump="sources/zacken/tests/integration/cases/zacken.tar.gz"

    if [ ! -f "$zacken_db_dump" ]; then
        printf "Could not find the Zacken test database dump.\n" 2>&1
        printf "Expected it to be at %s.\n" "$zacken_db_dump" 2>&1
        printf "Did you clone the Zacken sources?\n" 2>&1
        exit 1
    fi

    for db in zacken zacken-test; do
        printf "Restoring $db\n"
        gunzip --stdout "$zacken_db_dump" |\
        docker compose exec \
            --user postgres \
            --no-TTY \
            postgres \
        pg_restore \
            --disable-triggers \
            --data-only \
            --dbname $db \
            -U postgres
    done
