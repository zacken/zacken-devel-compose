#!/bin/sh

# SPDX-FileCopyrightText: 2024 Marten Ringwelski
# SPDX-FileContributor: Marten Ringwelski <git@maringuu.de>
#
# SPDX-License-Identifier: AGPL-3.0-only

set -e

if [ ! -f  /schema.sql ]; then
    printf "Expected a Zacken database schema at /schema.sql but didn't find any." 1>&2
    exit 1
fi

createdb zacken
psql -d zacken < /schema.sql

createdb zacken-test
psql -d zacken-test < /schema.sql
